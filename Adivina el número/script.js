const codigo = [];
const maxIntento = 8;
let intentos = 0;
let finPartida = false;

/*1. Genera una constante CODIGO_SECRETO de tipo array de 5 número aleatorios entre 0 y 9 usando la libreria Math.random();*/
function codigoSecreto() {
    for (let i = 0; i < 5; i++) {
        codigo[i] = Math.floor((Math.random() * 10));
    }
}

//Chivato para mostrar el código

function mostrarCodigoSecreto() {
    console.log(codigo);
}

codigoSecreto();
mostrarCodigoSecreto();


// Comprobar si la respuesta del usuario es correcta
function Comprobar() {

    if (finPartida){
        return;
    }

    let numero = document.getElementById("numero").value;
    let aciertos = 0;
    let resultado = "";

    //crear un fragment
    let fragment = document.createDocumentFragment();

    //dins del fragment creo rowResult
    let rowResult = document.createElement("div");
    rowResult.className = "rowResult w100 flex wrap";


    //dins de rowResult creo w20 x 5
    for (let i = 0; i<5; i++){
        let w20 = document.createElement("div");
        w20.className = "w20";
        let celResult = document.createElement("div");
        celResult.className = "celResult flex";
        
        w20.appendChild(celResult);
        rowResult.appendChild(w20);

    }

    fragment.appendChild(rowResult);
    document.getElementById("Result").prepend(fragment);

    // Mostrar el número ingresado por el jugador
    let rowInput = document.getElementsByClassName("rowResult")[0];
    let celInput = rowInput.getElementsByClassName("celResult");

    for (let i = 0; i < 5; i++) {
        
        celInput[i].innerHTML = numero[i];
    }    

    // Comprobar si cada dígito del número ingresado es igual al número secreto en la misma posición
    for (let i = 0; i < 5; i++) {

        // Pintar de gris oscuro si el número no está
        celInput[i].style.backgroundColor = "#333333";


        if (numero[i] == codigo[i]) {
            aciertos++;
            resultado += numero[i];
            // Pintar de verde si está en la posición correcta
            celInput[i].style.backgroundColor = "green";
        } else {

            // Comprobar si el dígito está en el código secreto
            for (let j = 0; j < 5; j++) {
                if ((numero[i] == codigo[j]) && (numero[j] != codigo[j])) {
                    // Pintar de amarillo si está pero en posición errónea
                    celInput[i].style.backgroundColor = "yellow";
                }
                    
            }
        }
    } 

    
    // Mostrar el resultado
    intentos++;
    if(aciertos == 5 || intentos == maxIntento){
        finPartida = true;
        let filaCodigo = document.getElementsByClassName("codigoSecreto")[0];
        let celCodigo = filaCodigo.getElementsByClassName("cel");

        for (let i = 0; i < 5; i++) {

            celCodigo[i].innerHTML = codigo[i];
        }
    }

    
    // Mostrar mensaje de acierto o de error
    const info = document.getElementById("info");
    

    if (aciertos == 5) {
        info.innerHTML = "¡Felicidades, has adivinado el número secreto en " + intentos + " intentos!";
        
    } else {
        info.innerHTML = "Seguir intentando, sólo has acertado " + aciertos + " dígitos. Intento número: " + intentos;
    }

    if(intentos >= maxIntento) {
        info.innerHTML = "Has alcanzado el número máximo de intentos permitidos.";
    }

    document.getElementById("numero").value = "";
}